---
title: "About"
description: "Megan Danelz, MT-BC, NICU-MT is a board certified music therapist based in the Greenville, SC area. She has experience working in the NICU, memory care facilities and in others."
keywords: [
    "music",
    "therapy",
    "care",
    "therapist",
    "working",
    "association",
    "children",
    "music therapy",
    "music therapist",
    "neonatal intensive care unit",
    "NICU",
    "therapy association",
]
lua:
  author: "Megan Danelz"
---

# Owner and Founder
### Megan Danelz, MT-BC, NICU-MT
Music Therapist Board Certified
Neonatal Intensive Care Unit Music Therapist

Megan Danelz, MT-BC, NICU-MT is a board certified music therapist based in the **Greenville, SC** area. She has experience working in the Neonatal Intensive Care Unit (NICU), memory care facilities and nursing homes, and in other medical settings. Megan also has experience working with children and adults with developmental delays, children who are at-risk, substance abuse, mental health, and individuals with physical disabilities. Prior to launching Heart Strings Music Therapy, Megan was a full time music therapist at Key Changes Therapy Services in Columbia, South Carolina. There, she **worked alongside Speech and Occupational therapy** and managed a caseload of approximately 23-27 clinical hours working specifically with children ages 1-18 years. Her caseload was comprised of a wide range of diagnoses including Autism Spectrum Disorder (ASD), Down Syndrome, Cerebral Palsy, genetic disorders, sensory processing disorders, and speech and language difficulties. Megan also was responsible for leading several groups at memory care facilities, a siblings group, and summer camp. In additional providing direct services, Megan also has extensive experience working as part of an interdisciplinary team by consulting, treating, and collaborating with other therapeutic disciplines and educational professionals. 

 A 2015 graduate of **Converse College** in Spartanburg, SC, Megan earned her Bachelor of Music degree with honors in Music Therapy. As part of her degree program, Megan completed a six month internship at the STAR Center of Jackson, Tennessee. Megan has successfully maintained certification through the Certification Board for Music Therapists (CBMT) since 2016. In addition to her degree, Megan has completed additional training for the Neonatal Intensive Care Unit (NICU-MT). 

Megan is an actively involved member of the [American Music Therapy Association](https://www.musictherapy.org/), [Music Therapy Association of South Carolina](https://www.facebook.com/Music-Therapy-Association-of-South-Carolina-258818369886/), and the [South East Region of the American Music Therapy Association](https://www.ser-amta.org/). 



## What is Music Therapy? 
Music Therapy is an **established health care profession** which provides clinical and evidence-based use of music interventions to accomplish individualized goals within a therapeutic relationship by a credentialed professional who has completed an approved music therapy program [(AMTA, 2005)](http://www.musictherapy.org/about/musictherapy/).

Through the use of music based strategies and interventions, music therapists work with individuals of all ages and abilities to master non-musical goals. **Music therapy can address needs within the cognitive, emotional, physical, spiritual, sensory, and social domains.**
