---
title: "Top 10 Go-To Instruments for Music Therapy"
description: "Which instruments do you use the most and recommend I should have in my collection or at home? Here’s a list of my top 10 go-to instruments for music therapy sessions. Happy music making!"
keywords: [
    "instruments",
    "music",
    "instrument",
    "songs",
    "shake",
    "rhythm",
    "music therapy",
    "motor work",
]
date: "2018-10-11T13:50:46+02:00"
tags: [
    "therapy",
]
banner: "img/banners/instruments.jpg"
lastmod: "2018-10-21T13:50:46+02:00"
lua:
  image:
    url: "img/banners/instruments.jpg"
    width: 800
    height: 600
  author: "Megan Danelz"
---
From parents and clients to music therapy students and interns, I have been asked over and over again:

 **“Which instruments do you use the most and recommend I should have in my collection or at home?”**

There’s a common misconception that a person must have some sort of musical background or training to receive music therapy services. I am here to tell you that is absolutely false. Whether someone grew up singing in church choirs or has had no musical experience whatsoever, music can still be a tool to help them reach their fullest potential. Almost everyone has had some experience with music. Just think about it, how did you learn the ABC’s? Probably a song, right? Most people also enjoy listening to some tunes while they drive, travel, exercise, or work. 

The beauty of music therapy is that there is no musical training required. We explore activities such as looking at words of songs and discussing meaning, writing our own songs for self- expression, active music listening, movement to music, and instrument playing, which brings me to the point of this blog. There are a few instruments that I use almost every session. My go-to instruments are easy to play, fun, and require no prior training. Once new instruments are introduced in a session, parents constantly ask where they can find these instruments to use them at home.

Here’s a list of my top 10 go-to instruments for music therapy sessions. Happy music making!

1. **Guitar** I use this instrument for almost every intervention presented in the session. Also, clients enjoy reaching out to strum the strings while I form the chords while singing their favorite songs.
1. **Cabasa** Remember old washboards? Cabasas make similar sounds. These instruments are awesome sensory stimulators. They feel cool on palms, arms, legs, and backs with the rolling beads and have been used to calm overstimulation. Cabasas have been used in sound discrimination and attention span work. They can also be used in fine and gross motor work such as palmar grasp, bilateral movement, and coordination.
1. **Castanets** Speaking of motor work, castanets are great fine motor tools. Keep a steady rhythm while also strengthening your tripod grasp. 
1. **Egg Shakers** Great fun! These tiny instruments can be used in so many interventions such as games, music making, hide and seek, motor work, etc. Shake, Shake, Shake those eggs!
1. **Tambourine** Such a cool rattling instrument. Shake it above your head, on your head, on your knee, on your hip, and in the air. This is also an easy instrument to hold, even for little hands.  
1. **Hand drum or gathering drum** A great instrument to work on socialization. Let’s get your whole family/peer group around a big gathering drum for some rhythm fun.
1. **Bells** Jingle bell, jingle bells, jingle all the way! Not just for Christmas songs, these are great instruments that can be adaptive for those friends who may not necessarily have complete mobility of their hands. My bells can be velcroed around wrists or ankles!
1. **Rhythm Sticks** I love using rhythm sticks to work on following directions and simple commands. These instruments can be tapped together, on the floor, scraped together, or even waved in the air. 
1. **Maracas** Similar to the shaker eggs, but larger, maracas are great to use when working on the palmer grasp and upper body gross motor skills. 
1. **Rainstick** Who doesn’t love the sounds of soothing rain? Rainsticks are a great instrument to calm overstimulation or to increase attention.
1. **Bonus**! Clatterpillar- A picture should suffice.

    ![Clatterpillar](/img/cp.jpg "Clatterpillar")

[Contact us]({{< ref "/contact" >}} "Contact us") today to schedule your music therapy evaluation. I’ll arrive to the eval with a guitar strapped to my back and several bags loaded down with my go-to instruments! 
