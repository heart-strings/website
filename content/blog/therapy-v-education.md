---
title: "The Difference in Music Therapy vs. Music Education"
description: "Although music therapy is an up-and-coming profession, some people still have trouble discerning the difference in music therapy and music education."
keywords: [
    "music",
    "therapy",
    "education",
    "therapist",
    "lessons",
    "goals",
    "client",
    "music therapy",
    "music education",
    "music therapist",
]
date: "2018-10-06T13:50:46+02:00"
tags: [
    "education",
    "therapy",
]
banner: "img/banners/gathering_drum.jpg"
lastmod: "2018-10-21T13:50:46+02:00"
lua:
  image:
    url: "img/banners/gathering_drum.jpg"
    width: 800
    height: 600
  author: "Megan Danelz"
---
Although music therapy is an up-and-coming profession, some people still have trouble discerning the difference in music therapy and music education. I have been asked numerous times, “why should my child receive music therapy when they have a music class at school” or “my child already takes piano lessons so why would they need music therapy too?” 

Well, the difference is clear when we take away the word music.

**Therapy**: Noun. The treatment of disease or disorders, as by some remedial, rehabilitating, or curative process (dictionary.com). 

**Education**: Noun. The act or process of acquiring general knowledge. The field of study that is concerned with the pedagogy of teaching and learning (freedictionary.com). 


What it boils down to is that music therapy is focusing on functional, non-musical skills, while music education is focusing on musical skills. Music therapy is defined as the “clinical and evidenced-based use of music interventions to accomplish individualized goals within a therapeutic relationship” (musictherapy.org) whereas music education emphasises music training in general music, singing, or instruments. The reason this is sometimes indiscernible to the outside observer is because music therapy and music class can look and sound very similar. 

For example, a music therapist might use the popular children’s song “Apples and Bananas” in session with a client who has speech and language delays. The therapist may emphasize the vowel sounds in the song to address vocalization in a safe and fun environment. Another example may be that a music therapist might use a drum in session with a client who has gross motor delays. The therapist may vary the type of hand drum used and the distance from client to drum in order to increase range of mobility in the upper body extremities. 

Music Therapy goals may be similar to those of physical therapy, occupational therapy, and speech therapy but the difference is the modality and the music based strategies and intervention to address the therapeutic goals. Music education may follow a general curriculum and typically does not address specific areas of deficit, which is the main focus of music therapy. 

Here at Heart Strings, we also offer adaptive music lessons which do have a similar focus as music education but with a music therapist’s touch. Your music therapist will adapt the teaching method to meet your individualized learning needs. 

[Contact us]({{< ref "/contact" >}} "Contact us") to schedule your music therapy evaluation or begin adaptive music lessons today! 