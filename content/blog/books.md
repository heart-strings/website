---
title: "Music Therapy and Books! Singable Song Stories"
description: "The cow said neigh and the pig said oink? Yup, you read that correctly. The cat did say hello and the farmer said moo but have you ever seen a bear combing his hair or a whale with a polka dotted tail down by the bay? I sure have! "
keywords: [
  "music",
  "therapy",
  "music therapy",
  "music therapist",
  "book",
  "books",
  "client",
  "sessions",
  "work",
  "skills",
  "distress tolerance",
  "in sessions"
]
date: "2019-05-28T13:50:46+02:00"
tags: [
    "education",
]
banner: "img/books/the-cow-said-neigh.jpg"
lua:
  image:
    url: "img/books/the-cow-said-neigh.jpg"
    width: 800
    height: 600
  author: "Megan Danelz"
---

The cow said neigh and the pig said oink? Yup, you read that correctly. The cat did say hello and the farmer said moo but have you ever seen a bear combing his hair or a whale with a polka dotted tail down by the bay? I sure have! 

I love using books or song books in music therapy sessions. Books such as “The Cow Said Neigh” by Rory Feek and “Down by the Bay” by Raffi can be used to work on so many more skills than just reading. They can be used to treat several goals that include visual stimulation, attention, socialization, creativity, expressive communication and language, distress tolerance, and memory recall. Of course, just like the music used in sessions, books are client preferred and age appropriate. Lots of books can be used in many different ways and for a variety of clients. For example, I might use the book “Brown Bear, Brown Bear What Do You See” by Bill Martin Jr. with a three year old client to work on animal and color identification, animal sounds, attention to task, and following directions. I could also use the same book with a 8-11 year old client to address expressive communication, distress tolerance and creativity by writing our own version of the book. Pink flamingo, pink flamingo, what do you see? Fun, right? 

{{< gallery dir="/img/books/" />}} {{< load-photoswipe >}}

Some books that I use originated as songs and then illustrated like “The Wheels on the Bus” or “Farmer in the Dell” and others I come up with a simple tune myself. One of the newer books that I have added to my collection is “The Cow Said Neigh” by Rory Feek. I use this book in sessions to address attention to task, animal identification, and distress tolerance. Sometimes clients have difficulty singing or reading about animals that make sounds other than the sound they are supposed to make. It can sometimes be tough to change a situation or alter what we know. Distress tolerance skills are used to help us cope when things do not go as planned or even in crisis. These skills also help us tolerate short or long term anxiety. The book, “The Cow Said Neigh” is a great book to work on these skills because the book definitely does not go to plan. The animals say things they are not supposed to say and everything seems to be a mess! Singing this book allows us to work on goals while also having fun and making music. 

Have you ever seem a fly wearing a tie? “Down by the Bay” by Raffi is another one of my favorite books. This is a great book to use when we are working on expressive communication and asking simple “wh” questions. It’s also a great book to be a little silly with! A goose kissing a moose down by the bay? What?! The tune of this book is simple and captivating enough to address an attention to task goal but is complex enough to be able to work on communication skills by asking “wh” questions. For example, after singing “have you ever seen a bear combing his hair” I might ask the client “Where is the bear combing his hair?” or “What is the bear doing?” The client could use context clues on the page to determine that the bear is combing his hair in the bathroom. This book is a huge hit in sessions! 

More of my favorite books to use include “There Was an Old Lady Who Swallowed a Fly” by Lucille Colandro and the other books where that silly old lady swallows things. Lucille Colandro wrote sequels to the original book for almost all of the holidays and seasons. These are great to use for sequencing, ordinal positioning, and memory recall. After singing through the book, I might ask the client “what did the old lady swallow first, second, third?” It’s just a downright silly book and never fails to get us giggling.

Some other singable story books I like to use include “Five Green and Speckled Frogs,” “Singing in the Rain,” “Old McDonald Had a Farm,” “Who Took the Cookie from the Cookie Jar,” and “Bear Hunt.” Comment below if you would like to know how I use them in sessions.

What’s your favorite children’s book?
