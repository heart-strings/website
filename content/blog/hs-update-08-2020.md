---
title: "Heart Strings Update"
description: "Your safety, and especially the safety of our most vulnerable, immunocompromised clients is our number one priority which is why we have crafted a strategic plan and safety protocol to try and keep everyone as safe as possible during in-person sessions. "
keywords: [
  "clients",
  "sessions",
  "therapist",
  "families",
  "music",
  "covid-19",
  "covid19",
  "covid 19",
  "contac",
  "in-person sessions",
  "clients families",
  "heart strings",
  "families therapists",
  "music therapy",
  "therapy services",
  "sessions postponed",
  "quarantine",
  "social distancing",
]
date: "2020-08-27T13:50:46+02:00"
tags: [
    "updates",
]
banner: "img/live_studio.jpg"
lastmod: "2020-08-27T13:50:46+02:00"
lua:
  image:
    url: "img/live_studio.jpg"
    width: 800
    height: 600
  author: "Megan Danelz"
---

### Friends & Families of Heart Strings,

We want to thank you so much for bearing with us as we move into uncharted territory and figure out what our new operations look like. We hope everyone is staying healthy and safe! 

With all things considered, your safety, and especially the safety of our most vulnerable, immunocompromised clients is our number one priority which is why we have crafted a strategic plan and safety protocol to try and keep everyone as safe as possible during in-person sessions. 


Here’s a glimpse of what strategies and protocols we have put in place in order to resume and maintain individual and group sessions.

- We ask that all clients, families, and therapists sign a risk acknowledgement waiver before resuming in-person sessions. The staff of Heart Strings Music Therapy Services, LLC is attempting to do everything possible to protect our clients, families, and therapists from Covid 19. However, we clearly cannot protect from all possible risks due to the nature of this threat and the nature of our clients needs. 

- Sessions will be postponed if anyone involved in sessions (families/clients/therapist) has been diagnosed with COVID-19 unless they have been cleared as fully recovered (minimum of a 10 day quarantine and symptom free). 

- There will be health screenings before each scheduled session which will include a brief questionnaire to report any current symptoms and temperature check. 

- Sessions will be postponed if therapist, client, or family has been knowingly exposed to COVID-19. 

- We also ask that anyone who has knowingly come in contact with Covid-19 will inform therapist as soon as possible and understand that sessions may be rescheduled or paused until negative Covid-19 test results or a 10 day quarantine with no symptoms. 


**Our Promise to You**

- Your therapist will remain vigilant in cleaning all surfaces and maintaining social distancing when possible. 

- Your therapist will thoroughly disinfect all instruments and materials after each use and will limit sharing of instruments/materials.

- Therapist and clients/families will wash hands before and after each held session. 

- Therapists and clients may use hand sanitizer or wash hands during a session at any point. 

- Group sizes will remain under 6 participants when space allows for social distancing.


*We are so excited to see you in person again!* However, we are still offering virtual visits if you and your family are not ready to resume in-person sessions yet. 

### Bonus news!! 
During the lockdown, we had so much fun with our weekly Facebook LIVE Rock n Roll Music and Songbook Story time! Thank you to everyone who joined us LIVE and made music with us! All videos can still be accessed on our facebook at heartstringsmts.com. We have also been working on recording some of our favorite songs and interventions that will be accessible on our new YouTube Channel. Please stay tuned for more information on how we will be utilizing our YouTube channel for our clients and potential clients. 
If you know of anyone who is interested in music therapy services please [contact us]({{< ref "/contact" >}} "contact us") today! 
