---
title: "Becoming a Music Therapist, MT-BC"
description: "Which instruments do you use the most and recommend I should have in my collection or at home? Here’s a list of my top 10 go-to instruments for music therapy sessions. Happy music making!"
keywords: [
  "music",
  "therapy",
  "board",
  "therapist",
  "therapists",
  "internship",
  "coursework",
  "music therapy",
  "music therapist",
  "music therapists",
  "board certification",
]
date: "2018-10-30T13:50:46+02:00"
tags: [
    "therapy",
]
banner: "img/banners/mt_grid.jpg"
lastmod: "2018-10-21T13:50:46+02:00"
lua:
  image:
    url: "img/banners/mt_grid.jpg"
    width: 800
    height: 600
  author: "Megan Danelz"
---

Music Therapists are certified to work with people of all ages with various disabilities. A Career in Music Therapy offers challenge, opportunity, and remarkable rewards but it is not an easy, breezy path to Board Certification. It takes years of education and training, clinical hours, an intensive internship, board exams, and a career of learning to become an MT. 

Some believe that it takes a "special person" to be a therapist and I believe that to be true. There are several personal qualifications that a music therapist must possess in order to be healthcare professional. Personal qualifications of a Music Therapist include a genuine interest in people and a desire to help others reach their fullest potential. The essence of music therapy practice involves establishing caring and professional relationships with people of all ages and abilities. As well as proficient musical skills and a love of music, an MT must also exhibit empathy, patience, creativity, imagination, flexibility, an openness to new ideas, an understanding of oneself, and a drive to complete educational requirements and uphold the standards of the profession. 

According to the [American Music Therapy Association](http://www.musictherapy.org/) (AMTA), curriculum for a music therapy degree includes coursework in music therapy, psychology, music, biological, social and behavioral sciences, disabling conditions, and general education studies. In conjunction with coursework, music therapists are required to be proficient in piano, guitar, voice, and percussion techniques which are most frequently used in therapy. During the four years of collegiate coursework, music therapy students must complete 1200 hours of clinical training. This includes placement in school, hospital, memory care, and mental health settings with experience in leading individual and group sessions. Once coursework has been completed, a student must then complete a 6+ month unpaid internship while taking on all responsibilities of acting music therapist. After coursework and internship is complete, there is one more giant leap to becoming board certified and that is sitting for the Board Exam. According to the Certification Board for Music Therapists (CBMT), a music therapist is initially board certified by successfully passing the board certification examination. The newly-credentialed music therapist receives a certificate from CBMT that indicates valid board certification for five years. The MT-BC credential subjects the certificant to the rules and regulations set forth by the CBMT Board of Directors in accordance with NCCA Program Accreditation Standards.

Music therapists are employed in many different settings including general and psychiatric hospitals, community mental health facilities, rehabilitation centers, day care facilities, memory care facilities, schools and private practice. Music therapists provide services for adults & children with psychiatric disorders, cognitive and developmental disabilities, speech and hearing impairments, physical disabilities, and neurological impairments, among others. Music therapists are usually members of an interdisciplinary team who support the goals and objectives for each client within the context of the music therapy setting.

Pretty impressive, huh? I am a proud Converse College alum and I take any opportunity I can to advocate for the music therapy department at Converse. The school is located in Spartanburg, SC and is one of two colleges in South Carolina that offer a bachelor’s degree in Music Therapy. The other school being Charleston Southern University in North Charleston, South Carolina. After my four years at Converse, I moved 8 hours away to Jackson, Tennessee to complete my 6 month internship at the STAR Center. There, my supervisors helped fuel my passion for music therapy and helped me land my first job only three months into my internship. I sat for my board exam almost immediately after completing my internship and finally became Megan Danelz, MT-BC (Music Therapist- Board Certified). 

Visit the Heart Strings contact page if you are interested in becoming a music therapist or if you want to meet an MT-BC! 

Stay tuned to learn more about additional certifications and happenings in the music therapy world. 
