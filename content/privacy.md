+++
title = "Privacy Policy"
+++

# Privacy Notice
This privacy notice discloses the privacy practices for (https://heartstringsmts.com/). This privacy notice applies solely to information collected by this website. Your invormation will only be used for the following:

1. Reaching out to respond to inqueries.
1. Following up with any questions.

### Information Collection, Use, and Sharing 
We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request.

We will not contact you via email in the future to tell you about specials, new products or services.

### Your Access to and Control Over Information 
You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email.

### Security 
We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.

While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.

If you feel that we are not abiding by this privacy policy, you should contact us immediately.