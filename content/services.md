---
title: "Who do Music Therapist Serve?"
description: "Music Therapists serve individuals with a wide range of ages and abilities. These include"
keywords: [
    "music",
    "services",
    "lessons",
    "weekly",
    "sessions",
    "group",
    "social",
    "music lessons",
    "60 minutes",
    "early intervention",
    "learning needs",
    "individual services",
    "group services",
    "Autism Spectrum Disorder (ASD)",
    "Down Syndrome",
    "Intellectual and Developmental Delays",
    "Early Intervention",
    "Cognitive Disabilities",
    "Physical Disabilities",
    "Emotional and Behavioral Challenges",
    "Social and Learning Needs",
    "Individual Services",
    "Group Services",
    "Adaptive Music Lessons",
    "NICU/Medical Music Therapy Services",
    "Memory Care",
    "In-Service and Presentations",
    "Attention to task",
    "Expressive communication",
    "Attention to task",
    "Behavior skills",
    "Cognitive skills",
    "Sensory issues",
    "Fine & gross motor skills",
    "Self-expression",
    "Social skills",
    "Group Services",
    "Social interactions",
    "Expressive communication",
    "Following directions",
    "Turn-taking",
    "Attending to task",
]
lua:
  author: "Megan Danelz"
---

##### Music Therapists serve individuals with a wide range of ages and abilities. These include:

* Autism Spectrum Disorder (ASD)
* Down Syndrome
* Intellectual and Developmental Delays
* Early Intervention
* Cognitive Disabilities
* Physical Disabilities
* Emotional and Behavioral Challenges
* Social and Learning Needs

##### Services
* Individual Services
* Group Services
* Adaptive Music Lessons
* NICU/Medical Music Therapy Services
* Memory Care
* In-Service and Presentations

##### Individual Services
Private one-on-one sessions are provided in the comfort of your home or an otherwise specified location. Sessions range from 30, 45, and 60 minutes and can be provided on a weekly, bi-weekly, or monthly basis. 

##### Goals are outlined by an individualized treatment plan developed through an initial assessment and may address the following areas: 
* Attention to task 
* Expressive communication
* Attention to task
* Behavior skills
* Cognitive skills 
* Sensory issues
* Fine & gross motor skills
* Self-expression
* Social skills
* Group Services

##### Group music
Group music therapy sessions are provided for groups of 4 or more participants.  Sessions are provided within community agencies and facilities that cater to multiple clients with similar needs. Groups may last for 30, 45, or 60 minutes and can be provided on a weekly, bi-weekly or monthly basis.

##### Interventions address a wide range of functional areas with an emphasis on: 
* Social interactions
* Expressive communication
* Following directions
* Turn-taking 
* Attending to task 

Interventions may include active music-making, lyric analysis, movement to music, learning songs about social, behavioral, and cognitive skills, as well as songwriting collaboration. Client preferences are key in developing effective music interventions.

##### Adaptive Lessons
Adaptive Music Lessons- Music lessons with a music therapist’s touch where teaching methods are adapted to meet your learning needs. Applied music lessons on guitar, piano, ukulele, and voice. 



### Research
* [Autism](http://www.musictherapy.org/assets/1/7/MT_Autism_2012.pdf)
* [Alzheimer’s](https://www.musictherapy.org/assets/1/7/MT_Alzheimers_2006.pdf)
* [Medical Population](https://www.musictherapy.org/assets/1/7/MT_Medicine_2006.pdf)
* [Early Intervention](https://www.musictherapy.org/assets/1/7/MT_Young_Children_2006.pdf)
* [MT in the News](https://www.musictherapy.org/events/media/)

	
