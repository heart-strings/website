---
id: "contact"
title: "Contact"
description: "Contact us to schedule your music therapy evaluation or begin adaptive music lessons today!"
keywords: [
    "music",
    "therapy",
    "contact",
    "therapist",
    "music therapy",
    "music therapist",
]
lua:
  author: "Megan Danelz"
---

# We are here to help you

Please provide your contact information to receive more information or schedule services.
